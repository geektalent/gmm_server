package com.geeklabs.gmm.server;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
/**
 * Contains project constants.
 */
public final class GMMServer {
	public static final String GMM_PROPERTIES_FILE_PATH = "c:/gmm/gmm.properties";
	
	public static final String TEST_KEY = "Test key"; // please don't change it, Same value also used in apk form activity class  
	public static final String GOOGLE_PLAY_SERVICES_LIB = "google-play-services_lib";
	public static final String GMMMAPV2_ZIP_RESOURCE_NAME = "GMMMapv2";
	
	public static final String START_ACTIVITY_RELEASE_UNSIGNED_APK = "StartActivity-release-unsigned.apk";
	public static final String GMM_MAPV2_ANDROID_MANIFEST_XML_PATH = "/GMMMapv2/AndroidManifest.xml";
	
	public static final String KEY_STORE_PWD = "geek";
	public static final String KEY_PWD = "geek";

	public static final String MARKER_INFO_SCRIPT_FILE_NAME = "markInfo.gmm";
	public static final String APP_INFO_SCRIPT_FILE_NAME = "appInfo.gmm";
	public static final String DRAW_BOUNDARY_SCRIPT_FILE_NAME = "boundary.gmm";

	public static final String APK_REPO_PATH = PropertiesManager.getProperty("apk_repo_path");
	public static final String GMM_SOURCE_PATH = APK_REPO_PATH + File.separator + "gmm_sources";
	public static final String GMM_MAPV2_SOURCE_PATH = GMM_SOURCE_PATH + File.separator + GMMMAPV2_ZIP_RESOURCE_NAME;
	public static final String GMM_GOOGLE_SER_LIB_SOURCE_PATH = GMM_SOURCE_PATH + File.separator + GOOGLE_PLAY_SERVICES_LIB;
	
	public static final String MANIFEST_PACKAGE = "package=\"com.geeklabs.mapmaker\"";
	public static final String MANIFEST_PACKAGE_MAPS_RECEIVE = "com.geeklabs.mapmaker.activity.permission.MAPS_RECEIVE";
	public static final String JAVA_PACKAGE = "com.geeklabs.mapmaker.R";
	
	public static final String IMAGES_DIR = "";
	
	private GMMServer() {
	}

	public static String getJavaSourcePath(String userAccount) {
		String userApkDirPath = getUserApkProjectDirPath(userAccount);
		return userApkDirPath + File.separator + "src";
	}
	
	public static String getUserUnsignedAPKPath(String userAccount, String apkNameWithExtention) {
		String userApkDirPath = getUserApkProjectDirPath(userAccount);
		return userApkDirPath + File.separator + "bin" + File.separator + apkNameWithExtention;
	}
	
	public static String getUserSignedAPKPath(String userAccount, String apkNameWithExtention) {
		String userApkDirPath = getUserApkRepoDirPath(userAccount);
		return userApkDirPath + File.separator + apkNameWithExtention;
	}
	
	public static String getUserAPKAndroidManifestPath(String userAccount) {
		String userApkDirPath = getUserApkProjectDirPath(userAccount);
		return userApkDirPath + File.separator + "AndroidManifest.xml";
	}
	
	public static String getUserAPKResStringPath(String userAccount) {
		String userApkDirPath = getUserApkProjectDirPath(userAccount);
		return userApkDirPath + File.separator + "res" + File.separator + "values" + File.separator + "strings.xml";
	}
	
	public static String getUserAPKResDrawablePath(String userAccount) {
		String userApkDirPath = getUserApkProjectDirPath(userAccount);
		return userApkDirPath + File.separator + "res" + File.separator + "drawable" + File.separator + "mm_icon.JPEG";
	}
	
	public static String getUserApkMarkingInfoFilePath(String userAccount) {
		return getUserApkProjectDirPath(userAccount) + File.separator + "assets" + File.separator + MARKER_INFO_SCRIPT_FILE_NAME ;
	}
	
	public static String getUserApkDrawBoundaryInfoFilePath(String userAccount) {
		return getUserApkProjectDirPath(userAccount) + File.separator + "assets" + File.separator + DRAW_BOUNDARY_SCRIPT_FILE_NAME ;
	}
	
	public static String getUserApkAppInfoFilePath(String userAccount) {
		return getUserApkProjectDirPath(userAccount) + File.separator + "assets" + File.separator + APP_INFO_SCRIPT_FILE_NAME ;
	}
	
	public static String getUserApkProjectDirPath(String userAccount) {
		return getUserApkRepoDirPath(userAccount) + File.separator + GMMMAPV2_ZIP_RESOURCE_NAME;
	}
	
	public static String getGooglePlayServiceLlibProjectDirPath(String userAccount) {
		return getUserApkRepoDirPath(userAccount) + File.separator + GOOGLE_PLAY_SERVICES_LIB;
	}
	
	public static String decodePath(String path) {
		try {
			return URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// We should never get here as this method using utf-8 encoding. ignore this exception.
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getUserApkRepoDirPath(String userAccount) {
		return decodePath(APK_REPO_PATH + File.separator + userAccount);
	}
}
