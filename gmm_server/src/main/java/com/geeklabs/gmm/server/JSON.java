package com.geeklabs.gmm.server;

import org.apache.commons.codec.binary.Base64;

public final class JSON {

	private JSON() {}
	
	public static byte[] decode(String s) {
	    return Base64.decodeBase64(s);
	}
	public static String encode(byte[] bytes) {
	    return Base64.encodeBase64String(bytes);
	}
	
	public static boolean validateIsNullOrEmpty(Object jsonValue, String userAccount) {
		if (jsonValue == null) {
			return true;
		} else {
			if (jsonValue instanceof String) {
				return ((String)jsonValue).isEmpty();
			}
		}
		Log.getLogger().severe("request parameter is invalid: '"+ userAccount + "', expected a not empty value "+ jsonValue);
		return false;
	}
}
