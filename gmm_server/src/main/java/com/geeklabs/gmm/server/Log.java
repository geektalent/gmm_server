package com.geeklabs.gmm.server;

import java.io.File;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class Log {
	/** Logger Object is creating Here */
	static Logger logger = Logger.getLogger("Make My mApp");
	static {
		try {
			String userdir = GMMServer.APK_REPO_PATH;

			FileHandler file = new FileHandler(userdir + File.separator + "log"
					+ ".html", true);
			file.setFormatter(new LogFormatter());
			file.setEncoding("UTF-8");

			logger.addHandler(file);
			logger.setUseParentHandlers(false);
		} catch (Exception e) {
			logger.severe("Could not create logger file..." + e + "\n");
		}
	}

	/**
	 * This method creates the logger object
	 * 
	 * @return logger object.
	 */
	public static Logger getLogger() {
		return logger;
	}
}
