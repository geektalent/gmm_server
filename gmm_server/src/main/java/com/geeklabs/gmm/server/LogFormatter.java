package com.geeklabs.gmm.server;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class LogFormatter extends java.util.logging.Formatter {

	public LogFormatter() {
	}

	// This method is called for every log records
	public String format(LogRecord rec) {
		StringBuffer buf = new StringBuffer(1000);
		// Bold any levels >= WARNING
		buf.append("<TR");
		if (rec.getSequenceNumber() % 2 != 0) {
			buf.append(" BGCOLOR='#D1E0E0' ");
		}
		buf.append(">");
		// Message Type Icon Starts

		if (rec.getLevel().intValue() == Level.WARNING.intValue()) {
			// Indicates Warning Steps
			buf.append("<TD>");
			buf.append("<b> <font color='blue'> &nbsp;");
			buf.append("&nbsp;");
			buf.append("<img src=\"" + GMMServer.IMAGES_DIR
					+ "/info.gif\">");
			buf.append("</font></b>");
			buf.append("</TD>");
		} else if (rec.getLevel().intValue() == Level.SEVERE.intValue()) // Indicates
																			// Severe
																			// Steps
		{
			buf.append("<TD>");
			buf.append("<b> <font color='Red'> &nbsp;");
			buf.append("&nbsp;");
			buf.append("<img src=\"" + GMMServer.IMAGES_DIR
					+ "/severe.gif\">");
			buf.append("</font></b>");
			buf.append("</TD>");
		} else if (rec.getLevel().intValue() == Level.INFO.intValue()) // Indicates
																		// Information
																		// Steps
		{
			buf.append("<TD>");
			buf.append("&nbsp;");
			buf.append("<img src=\"" + GMMServer.IMAGES_DIR
					+ "/info.gif\">");
			buf.append("</TD>");
		} else if (rec.getLevel().intValue() == Level.FINER.intValue()) // Indicates
																		// Manual
																		// Steps
		{
			buf.append("<TD>");
			buf.append("&nbsp;");
			buf.append("<img src=\"" + GMMServer.IMAGES_DIR
					+ "/manual.gif\">");
			buf.append("</TD>");
		} else {
			buf.append("<TD>&nbsp;");
			buf.append(rec.getLevel());
			buf.append("</TD>");
		}
		// Message Type Icon Ends

		// Message Type Starts
		if (rec.getLevel().intValue() == Level.WARNING.intValue()) // TODO
		{
			buf.append("<TD>");
			buf.append("<b> <font color='blue'> &nbsp;");
			buf.append("&nbsp;");
			buf.append(rec.getLevel());
			buf.append("</font></b>");
			buf.append("</TD>");
		} else if (rec.getLevel().intValue() == Level.SEVERE.intValue()) // TODO
		{
			buf.append("<TD>");
			buf.append("<b> <font color='Red'> &nbsp;");
			buf.append("&nbsp;");
			buf.append(rec.getLevel());
			buf.append("</font></b>");
			buf.append("</TD>");
		} else if (rec.getLevel().intValue() == Level.FINER.intValue()) {
			buf.append("<TD>&nbsp;");
			buf.append("<b> <font color='lightblue'> &nbsp;");
			buf.append("Manual Step");
			buf.append("</font></b>");
			buf.append("</TD>");
		} else {
			buf.append("<TD>&nbsp;");
			buf.append(rec.getLevel());
			buf.append("</TD>");
		}
		// Message Type Ends
		buf.append("<TD width='40%'>&nbsp;");
		buf.append(formatMessage(rec));
		buf.append("</TD>");

		buf.append("<TD>&nbsp;");
		buf.append(rec.getSourceClassName());
		buf.append("</TD>");

		buf.append("<TD>&nbsp;");
		buf.append(rec.getSourceMethodName());
		buf.append("</TD>");

		buf.append("<TD>&nbsp;");
		java.util.Date d = new java.util.Date(rec.getMillis());
		java.text.SimpleDateFormat header_sm1 = new java.text.SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		String _sdate = header_sm1.format(d);
		buf.append(_sdate);
		buf.append("</TD>");

		buf.append("</TR> \n");
		return buf.toString();
	}

	// This method is called just after the handler using this
	// formatter is created
	public String getHead(Handler h) {
		StringBuffer buf = new StringBuffer(1000);
		buf.append("<HTML><HEAD><TITLE>" + (new java.util.Date())
				+ "</TITLE></HEAD><BODY><BR>\n");

		buf.append("<TABLE WIDTH='740' CELLSPACING='0' CELLPADDING='0' BORDER='1px' BORDERCOLOR='#330000'> \n");

		buf.append("<TR BGCOLOR='#D1E0E0' >");
		buf.append("<TD colspan=6> <center> <b> Geek Labs 'Make My mApp' Logger</center> </b></TD> \n");
		buf.append("</TR>");

		buf.append("<tr></tr>");

		buf.append("<TR BGCOLOR='#F3EFE4' >");
		buf.append("<TD>  </TD> \n");
		buf.append("<TD><b> <center>Message Type </center> </b></TD> \n");
		buf.append("<TD width='40%'><b> <center> Description </center></b></TD> \n");
		buf.append("<TD> <b><center> Class </center></b></TD> \n");
		buf.append("<TD> <b><center> Method </center></b></TD> \n");
		buf.append("<TD> <b><center> Time </center></b></TD> \n");
		buf.append("</TR> \n");

		return buf.toString();
	}

	// This method is called just after the handler using this
	// formatter is closed
	public String getTail(Handler h) {
		return "</TABLE></BODY></HTML>\n";
	}
}