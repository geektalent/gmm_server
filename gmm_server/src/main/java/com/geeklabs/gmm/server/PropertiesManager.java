	package com.geeklabs.gmm.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {
	
	private static Properties properties = null;

	private PropertiesManager() {
	}

	public static Properties loadProperties() {
		if (properties == null) {
			properties = new Properties();
			try {
				FileInputStream inStream = new FileInputStream(GMMServer.GMM_PROPERTIES_FILE_PATH);

				properties.load(inStream);
			} catch (IOException e) {
				Log.getLogger().severe("'gmm.properties' file is missing at 'c:/gmm' location");
				e.printStackTrace();
				throw new IllegalStateException();
			}
		}
		return properties;
	}

	public static String getProperty(String key) {
		properties = loadProperties();
		return properties.getProperty(key);
	}
	public static void main(String[] args) {
		int i ='a';
		System.out.println(i);
	}
}