package com.geeklabs.gmm.server;

public class StringUtils {

	public static String replaceAll(String content, String replaceWith, String replaceTo) {
		return content.replaceAll(replaceWith, replaceTo);
	}
	
	public static String replaceAllWhiteSpaces(String content) {
		return content.replaceAll("\\s+","_");
	}
}
