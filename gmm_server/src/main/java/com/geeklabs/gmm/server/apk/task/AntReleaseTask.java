package com.geeklabs.gmm.server.apk.task;

import java.io.File;
import java.io.IOException;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.Log;
import com.geeklabs.gmm.server.StringUtils;

public class AntReleaseTask {
	
	public static void main(String[] args) {
		String s = new String("Geek Lab s");
		String replaceAll = s.replaceAll("\\s+","_");
		System.out.println(replaceAll);
	}

	public static void runApkReleaseAntTask(String userAccount) throws IOException, InterruptedException {
		String userApkDirPath = GMMServer.getUserApkProjectDirPath(userAccount);
		
		String logPath = GMMServer.APK_REPO_PATH + File.separator + userAccount + "-apk-release.txt";
		String cmd = userApkDirPath + "/apkrelease.bat " + userApkDirPath + " C: " + logPath;
		Log.getLogger().info("apk release command:" + cmd);
		Process process = Runtime.getRuntime().exec(cmd);
		process.waitFor();

		Log.getLogger().info("apk release command has been triggered for user: " + userAccount);
	}

	public static void runZipalignAntTask(String userAccount, String appName, String apkNameWithExtention) throws IOException, InterruptedException {
		String userSignedAPKPath = GMMServer.getUserSignedAPKPath(userAccount, apkNameWithExtention);
		String userApkDirPath = GMMServer.getUserApkProjectDirPath(userAccount);
		
		File file = new File(userSignedAPKPath);
		if(file.exists()) {			
			String logPath = GMMServer.APK_REPO_PATH + File.separator + userAccount + "-zipalign.txt";
			String cmd = userApkDirPath + "/zipaligntask.bat " + file.getParent() + " C:" +
					" \"" + apkNameWithExtention + "\" \"" + StringUtils.replaceAllWhiteSpaces(appName) + "-aligned.apk\" " + logPath;
			//"zipalign -f -v 4 "geek test-sigbne.apk"  geek-aligned.apk";
			
			System.out.println(cmd);
			Log.getLogger().info("apk zip align command:" + cmd);
			Process process = Runtime.getRuntime().exec(cmd);
			process.waitFor();
			
			Log.getLogger().info("apk zip align command has been triggered for user: " + userAccount);
		} else {
			Log.getLogger().severe("zip align not triggered as signed apk not generated " + userAccount);
		}

	}	
}
