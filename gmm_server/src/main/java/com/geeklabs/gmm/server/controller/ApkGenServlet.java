package com.geeklabs.gmm.server.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.lingala.zip4j.exception.ZipException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.JSON;
import com.geeklabs.gmm.server.Log;
import com.geeklabs.gmm.server.StringUtils;
import com.geeklabs.gmm.server.apk.task.AntReleaseTask;
import com.geeklabs.gmm.server.customization.ChangeAppName;
import com.geeklabs.gmm.server.customization.ChangeMapApiKey;
import com.geeklabs.gmm.server.customization.ChangePackage;
import com.geeklabs.gmm.server.customization.CreatingDrwableFolder;
import com.geeklabs.gmm.server.customization.CreatingSqlFiles;
import com.geeklabs.gmm.server.customization.InitCustomization;
import com.geeklabs.gmm.server.dao.EntityManagerProvider;
import com.geeklabs.gmm.server.dao.impl.ApkInfoDaoImpl;
import com.geeklabs.gmm.server.dao.impl.KeyGenDaoImpl;
import com.geeklabs.gmm.server.dao.impl.UserDaoImpl;
import com.geeklabs.gmm.server.domain.ApkInfo;
import com.geeklabs.gmm.server.domain.User;
import com.geeklabs.gmm.server.jarsigner.ApkSign;
import com.geeklabs.gmm.server.pojo.ApkGenerate;
import com.google.common.collect.Lists;

public class ApkGenServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		throw new UnsupportedOperationException("not supported");
	}

	@SuppressWarnings("unchecked")
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger logger = Log.getLogger();
		JSONObject jsonResponse = new JSONObject();
		EntityManager entityManager = null;
		try {		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		ApkGenerate apkGenerate = new ApkGenerate();
		String userAccount = null;
		byte[] iconBytes = null;
		try {
			BufferedReader reader = request.getReader();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;
			apkGenerate.setApiKey((String) jsonObject.get("apiKey"));
			apkGenerate.setAppName((String) jsonObject.get("appName"));
			apkGenerate.setKeyStoreName((String) jsonObject.get("keyStoreName"));
			apkGenerate.setPackageName((String) jsonObject.get("package"));
			
			if (JSON.validateIsNullOrEmpty(jsonObject.get("userAccount"), "userAccount")) {
				throw new IllegalStateException("User account is can not be empty");
			}
			userAccount = (String) jsonObject.get("userAccount");
			logger.info("Apk creation requested by user : " + userAccount);
			logger.info("Apk creating with details : " + apkGenerate);
			
			JSONArray msg = (JSONArray) jsonObject.get("markingData");
			ArrayList<String> listOfMarkers = Lists.newArrayList();
			for (Object s : msg) {
				listOfMarkers.add((String)s);
			}	
			apkGenerate.setMarkingInfoQueries(listOfMarkers);
			
			apkGenerate.setAppInfoQuery((String)jsonObject.get("appInfoQuery"));
			
			JSONArray jsonDrawBundaries = (JSONArray) jsonObject.get("drawBundaries");
			ArrayList<String> drawBundaries = Lists.newArrayList();
			for (Object s : jsonDrawBundaries) {
				drawBundaries.add((String)s);
			}
			apkGenerate.setDrawBoundaries(drawBundaries);
			
			Object appIcon = jsonObject.get("appIcon");
			if (appIcon != null) {
				String icon = (String) appIcon;
				iconBytes = JSON.decode(icon);
			}
		} catch (Exception e) {
			logger.severe("Problem occurred while reading apk info from request" + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", "Problem occurred while processing request, "
					+ "Please make sure you entered valid data or contact your administrator");
			response.getWriter().write(jsonResponse.toString());
			return ;
		}

		// Get User
		User user = null;
		// Get entity manager here, so that apk info save and user update will happen in same transaction
		entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
		try {
			user = UserDaoImpl.INSTANCE.getUser(userAccount, entityManager);
		} catch (Exception e) {
			logger.severe("Problem occurred while retrieving user info for user: " + userAccount + " and message:" + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", userAccount + " is not valid acccount. Please register");
			response.getWriter().write(jsonResponse.toString());
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
			return ;
		}
		
		// Validate user
		if (user == null) {
			logger.severe("Not able to retrieve user info for user: " + userAccount);
			jsonResponse.put("msg", userAccount + " is not valid acccount. Please register");
			response.getWriter().write(jsonResponse.toString());
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
			return ;
		}
		
		// Init customization
		try {
			// Before init , clean first 
			InitCustomization.clean(userAccount);
			
			InitCustomization.init(userAccount);
		} catch (ZipException e1) {
			logger.severe("Problem occurred while initializing user apk working directory " + e1.getMessage());
			e1.printStackTrace();
			jsonResponse.put("msg", "Problem occurred while processing request, try after some time");
			response.getWriter().write(jsonResponse.toString());
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
			return ;
		} 

		ChangeMapApiKey.changeMapApiKey(apkGenerate.getApiKey(), userAccount);// "AIzaSyBEJBnuV0ZD7aoExSFxPh4OSqV46eVdmNc");
		ChangeAppName.changeAppName(apkGenerate.getAppName(), userAccount);
		
		if (iconBytes != null) {
			CreatingDrwableFolder.createDrawableFolder(userAccount, iconBytes);
		}

		CreatingSqlFiles.generateMarkingInfo(userAccount, apkGenerate.getMarkingInfoQueries());
		CreatingSqlFiles.generateAppInfo(userAccount, apkGenerate.getAppInfoQuery());
		CreatingSqlFiles.generateDrawBoundary(userAccount, apkGenerate.getDrawBoundaries());

		// Change package name
		ChangePackage.changePackage(userAccount, apkGenerate.getPackageName());
		
		// creating apk using apk builder
		try {
			AntReleaseTask.runApkReleaseAntTask(userAccount);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			logger.severe("Problem occurred while building an apk: " + e1.getMessage());
			jsonResponse.put("msg", "Problem occurred while generating application, try after some time");
			response.getWriter().write(jsonResponse.toString());
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}			
			return;
		}

		
		// signing apk using jar signer
		String apkNameWithExtention = StringUtils.replaceAllWhiteSpaces(apkGenerate.getAppName()+ "-signed.apk");
		String userSignedAPKPath = GMMServer.getUserSignedAPKPath(userAccount, apkNameWithExtention);
		com.geeklabs.gmm.server.domain.KeyStore store = null;
		try {
			String userUnsignedAPKPath = GMMServer.getUserUnsignedAPKPath(userAccount, GMMServer.START_ACTIVITY_RELEASE_UNSIGNED_APK);
			
			String keyStoreName = apkGenerate.getKeyStoreName();
			if (GMMServer.TEST_KEY.equals(keyStoreName)) {
				store = KeyGenDaoImpl.INSTANCE.getDefaultKeyStore();
			} else {
				store = KeyGenDaoImpl.INSTANCE.getKeyStoreByName(user.getId(), keyStoreName);
			}
			
			ApkSign.signApk(apkGenerate, user, userUnsignedAPKPath, userSignedAPKPath, store);
		} catch (Exception e) {
			logger.severe("Problem occurred while signing apk: " + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", "Problem occurred while generating application, try after some time");
			response.getWriter().write(jsonResponse.toString());
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
			return ;
		}
		
		// call zipalign
		try {
			AntReleaseTask.runZipalignAntTask(userAccount, apkGenerate.getAppName(), apkNameWithExtention);
		} catch (InterruptedException e) {
			logger.severe("Problem occurred while zip aligning apk: " + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", "Problem occurred while generating application, try after some time");
			response.getWriter().write(jsonResponse.toString());
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
			return ;
		} 
		
		// Remove user apk project working folders
		InitCustomization.clean(userAccount);
		
		// Getting apk file from default path
		String userZipalignedAPKPath = GMMServer.getUserSignedAPKPath(userAccount, StringUtils.replaceAllWhiteSpaces(apkGenerate.getAppName()) + "-aligned.apk");
		byte[] binaryAPKFile = getApkAsBytes(userZipalignedAPKPath);

		ApkInfo apkInfo = new ApkInfo();
		apkInfo.setApkFileInfo(binaryAPKFile);
		apkInfo.setUserID(user.getId());
		apkInfo.setKeyId(store.getKeyId());
		apkInfo.setApkName(apkGenerate.getAppName());
		apkInfo.setPackageName(apkGenerate.getPackageName());
		apkInfo.setApiKey(apkGenerate.getApiKey());
		// Transaction starts here
		try {
			ApkInfoDaoImpl.INSTANCE.save(apkInfo, entityManager);
	
			// Update User about test key used info if key store name is Test key 
			if (GMMServer.TEST_KEY.equals(apkGenerate.getKeyStoreName())) {
				user.setTestKeyUsed(true);
				UserDaoImpl.INSTANCE.save(user, entityManager);
			}
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}				
		} // Transaction ends here
		
		jsonResponse.put("msg","success");
		jsonResponse.put("appId", 150);
		response.getWriter().write(jsonResponse.toString());

		logger.info("Completed apk creation for user : " + userAccount);
		System.out.println("Completed apk creation :).");
		} catch (Exception e) {
			logger.severe("Problem occurred while generating application: " + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", "Problem occurred while generating application, try after some time");
			response.getWriter().write(jsonResponse.toString());
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

	private byte[] getApkAsBytes(String userSignedAPKPath)
			throws FileNotFoundException, IOException {
		File apkFile = new File(userSignedAPKPath);
		byte[] binaryAPKFile = new byte[(int) apkFile.length()];

		FileInputStream fileInputStream = new FileInputStream(apkFile);
		// convert file into array of bytes
		fileInputStream.read(binaryAPKFile);
		fileInputStream.close();
		return binaryAPKFile;
	}
}
