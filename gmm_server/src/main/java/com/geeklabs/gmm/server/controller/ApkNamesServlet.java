package com.geeklabs.gmm.server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.geeklabs.gmm.server.JSON;
import com.geeklabs.gmm.server.Log;
import com.geeklabs.gmm.server.dao.EntityManagerProvider;
import com.geeklabs.gmm.server.dao.impl.ApkInfoDaoImpl;
import com.geeklabs.gmm.server.dao.impl.UserDaoImpl;
import com.geeklabs.gmm.server.domain.ApkInfo;
import com.geeklabs.gmm.server.domain.User;

/**
 * Servlet implementation class ApkNamesServlet
 */
public class ApkNamesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Logger logger = Log.getLogger();
		JSONObject jsonResponse = new JSONObject();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		EntityManager entityManager = null;
		try {
			BufferedReader reader = request.getReader();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;
			if (JSON.validateIsNullOrEmpty(jsonObject.get("userAccount"), "userAccount")) {
				throw new IllegalStateException("User account is null");
			}
			String userAccount = (String) jsonObject.get("userAccount");

			// Get User
			User user = null;
			try {
				entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
				user = UserDaoImpl.INSTANCE.getUser(userAccount, entityManager);
			} catch (Exception e) {
				logger.severe("Problem occurred while retrieving user info for user: " + userAccount + " and message:" + e.getMessage());
				e.printStackTrace();
				jsonResponse.put("msg", userAccount + " is not valid acccount. Please register");
				response.getWriter().write(jsonResponse.toString());
				return ;
			} finally {
				if (entityManager != null && entityManager.isOpen()) {
					entityManager.close();
				}
			}
			
			// Validate user
			if (user == null) {
				logger.severe("Not able to retrieve user info for user: " + userAccount);
				jsonResponse.put("msg", userAccount + " is not valid acccount. Please register");
				response.getWriter().write(jsonResponse.toString());
				if (entityManager != null && entityManager.isOpen()) {
					entityManager.close();
				}
				return ;
			}
			 
				int id = user.getId();
				List<ApkInfo> apkList = ApkInfoDaoImpl.INSTANCE.getApkList(id);

				Map<String, Long> apkNames = new HashMap<>();

				for (ApkInfo apkInfo : apkList) {
					apkNames.put(apkInfo.getApkName(), apkInfo.getApkID());
				}

				JSONObject jsonArray = new JSONObject(apkNames);
				response.getWriter().write(jsonArray.toJSONString());
			
		} catch (Exception e) {
			logger.severe("Problem occurred while retrieving application names: " + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", "Problem occurred while retrieving application names, try after some time or contact administrator");
			response.getWriter().write(jsonResponse.toString());
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

}