package com.geeklabs.gmm.server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.geeklabs.gmm.server.JSON;
import com.geeklabs.gmm.server.Log;
import com.geeklabs.gmm.server.dao.EntityManagerProvider;
import com.geeklabs.gmm.server.dao.impl.KeyGenDaoImpl;
import com.geeklabs.gmm.server.dao.impl.UserDaoImpl;
import com.geeklabs.gmm.server.domain.KeyStore;
import com.geeklabs.gmm.server.domain.User;
import com.geeklabs.gmm.server.jarsigner.keys.KeyToolManager;
import com.geeklabs.gmm.server.pojo.KeyInfo;

/**
 * Servlet implementation class KeyGenServlet
 */
@SuppressWarnings("serial")
public class KeyGenServlet extends HttpServlet {

	public KeyGenServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		throw new UnsupportedOperationException("not suported operation");
	}

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Logger logger = Log.getLogger();
		String userAccount = null;
		String defaultKey = null;
		JSONObject jsonResponse = new JSONObject();
		PrintWriter out = response.getWriter();
		EntityManager entityManager = null;
		try {
			BufferedReader reader = request.getReader();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;

			KeyInfo keyStore = new KeyInfo();

			if (JSON.validateIsNullOrEmpty(jsonObject.get("userAccount"), "userAccount")) {
				throw new IllegalStateException("User account is null");
			}
			userAccount = ((String) jsonObject.get("userAccount"));
			defaultKey =  ((String) jsonObject.get("defaultKey"));

			keyStore.setKeystoreName((String) jsonObject.get("keystoreName"));
//			keyStore.setKeyName((String) jsonObject.get("keyName"));
			keyStore.setCountryCode((String) jsonObject.get("countryCode"));
			
			String validity = (String) jsonObject.get("certValidityYears");
			if (validity != null && !validity.isEmpty()) {
				keyStore.setCertValidityYears(Integer.parseInt(validity));
			}
			
			keyStore.setCommonName((String) jsonObject.get("commonName"));
			keyStore.setOrganizationalUnit((String) jsonObject
					.get("organizationalUnit"));
			keyStore.setOrganization((String) jsonObject.get("organization"));
			// keyStore.setStreet((String) jsonObject.get("street"));
			keyStore.setLocality((String) jsonObject.get("locality"));
			keyStore.setState((String) jsonObject.get("state"));

			// Get User
			User user = null;
			try {
				entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
				user = UserDaoImpl.INSTANCE.getUser(userAccount, entityManager);
			} catch (Exception e) {
				logger.severe("Problem occurred while retrieving user info for user: " + userAccount + " and message:" + e.getMessage());
				e.printStackTrace();
				jsonResponse.put("msg", userAccount + " is not valid acccount. Please register");
				response.getWriter().write(jsonResponse.toString());
				return ;
			} finally {
				if (entityManager != null && entityManager.isOpen()) {
					entityManager.close();
				}
			}
			
			// Validate user
			if (user == null) {
				logger.severe("Not able to retrieve user info for user: " + userAccount);
				jsonResponse.put("msg", userAccount + " is not valid acccount. Please register");
				response.getWriter().write(jsonResponse.toString());
				if (entityManager != null && entityManager.isOpen()) {
					entityManager.close();
				}
				return ;
			}
						
			KeyStore store = new KeyToolManager().keyGenerator(keyStore);
			store.setUserId(user.getId());
			
			// calling dao layer
			KeyGenDaoImpl.INSTANCE.save(store);

			response.setContentType("application/json");

			jsonResponse.put("msg", "success");
			out.write(jsonResponse.toJSONString());
			out.flush();

		} catch (Exception e) {
			logger.severe("Problem occurred while creating key store: " + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", "Please make sure you entered valid data or contact your administrator");
			response.getWriter().write(jsonResponse.toString());
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}
}
