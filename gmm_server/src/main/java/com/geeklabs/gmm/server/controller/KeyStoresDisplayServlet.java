package com.geeklabs.gmm.server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.geeklabs.gmm.server.JSON;
import com.geeklabs.gmm.server.Log;
import com.geeklabs.gmm.server.dao.EntityManagerProvider;
import com.geeklabs.gmm.server.dao.impl.ApkInfoDaoImpl;
import com.geeklabs.gmm.server.dao.impl.KeyGenDaoImpl;
import com.geeklabs.gmm.server.dao.impl.UserDaoImpl;
import com.geeklabs.gmm.server.domain.ApkInfo;
import com.geeklabs.gmm.server.domain.KeyStore;
import com.geeklabs.gmm.server.domain.User;

public class KeyStoresDisplayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		Logger logger = Log.getLogger();
		JSONObject jsonResponse = new JSONObject();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		EntityManager entityManager = null;
		try {
			BufferedReader reader = req.getReader();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;
			if (JSON.validateIsNullOrEmpty(jsonObject.get("userAccount"), "userAccount")) {
				throw new IllegalStateException("User account is null");
			}
			String userAccount = (String) jsonObject.get("userAccount");
			
			// Get User
			User user = null;
			try {
				entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
				user = UserDaoImpl.INSTANCE.getUser(userAccount, entityManager);
			} catch (Exception e) {
				logger.severe("Problem occurred while retrieving user info for user: " + userAccount + " and message:"
						+ e.getMessage());
				e.printStackTrace();
				jsonResponse.put("msg", userAccount + " is not valid acccount. Please register");
				response.getWriter().write(jsonResponse.toString());
				return;
			} finally {
				if (entityManager != null && entityManager.isOpen()) {
					entityManager.close();
				}
			}

			// Validate user
			if (user == null) {
				logger.severe("Not able to retrieve user info for user: " + userAccount);
				jsonResponse.put("msg", userAccount + " is not valid acccount. Please register");
				response.getWriter().write(jsonResponse.toString());
				if (entityManager != null && entityManager.isOpen()) {
					entityManager.close();
				}
				return;
			}
			int id = user.getId();
			List<KeyStore> keyStores = KeyGenDaoImpl.INSTANCE.retrieve(id);

			Map<String, String> keyNames = new HashMap<>();

			for (KeyStore keyStore : keyStores) {
				ApkInfo apkInfo = ApkInfoDaoImpl.INSTANCE.getApk(id, keyStore.getKeyId());
				String value = keyStore.getSha1key();
				if (apkInfo != null) {
					value= value + "$@" +apkInfo.getPackageName() + "$@" +apkInfo.getApiKey();
				}
				keyNames.put(keyStore.getkeyStoreName(), value);
			}

			JSONObject jsonArray = new JSONObject(keyNames);
			response.getWriter().write(jsonArray.toJSONString());

		} catch (Exception e) {
			logger.severe("Problem occurred while retrieving key names: " + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", "Problem occurred while retrieving key names, try after some time or contact administrator");
			response.getWriter().write(jsonResponse.toString());
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

	/*
	 * public static void main(String[] args) { Map map = new HashMap<>();
	 * map.put("geek", "sha1"); map.put("geek1", "sha12");
	 * 
	 * JSONObject jsonObject = new JSONObject(map);
	 * 
	 * System.out.println(jsonObject.toString()); }
	 */

}
