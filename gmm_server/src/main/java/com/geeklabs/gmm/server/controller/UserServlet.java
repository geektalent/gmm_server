package com.geeklabs.gmm.server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.geeklabs.gmm.server.JSON;
import com.geeklabs.gmm.server.Log;
import com.geeklabs.gmm.server.dao.EntityManagerProvider;
import com.geeklabs.gmm.server.dao.impl.UserDaoImpl;
import com.geeklabs.gmm.server.domain.User;

public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		throw new UnsupportedOperationException("not suported");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Logger logger = Log.getLogger();
		JSONObject jsonResponse = new JSONObject();
		PrintWriter out = resp.getWriter();
		EntityManager entityManager = null;
		try {
			BufferedReader reader = req.getReader();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;

			
			if (JSON.validateIsNullOrEmpty(jsonObject.get("userAccount"), "userAccount")) {
				logger.severe("User acccount is null");
				jsonObject.put("msg", "Problem occurred while processing request, "
						+ "Please make sure you entered valid data or contact your administrator");
				out.write(jsonResponse.toJSONString());
				out.flush();
				return;
			}
			String email = (String) jsonObject.get("userAccount");
			
			entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
			
			User existingUser = UserDaoImpl.INSTANCE.getUser(email, entityManager);
			if (existingUser == null) {
				// Create user
				User user = new User();
				user.setEmail(email);
				user.setTestKeyUsed(false);
				UserDaoImpl.INSTANCE.save(user, entityManager);
				
				existingUser = user;
				logger.info("User successfully registered: " + email);
			}
			jsonResponse.put("msg", "success");
			jsonResponse.put("isTestKeyUsed", existingUser.isTestKeyUsed());
			out.write(jsonResponse.toJSONString());
			out.flush();
		} catch (Exception e) {
			logger.severe("Problem occurred while registering user " + e.getMessage());
			e.printStackTrace();
			jsonResponse.put("msg", "Problem occurred while processing request, "
					+ "Please make sure you entered valid data or contact your administrator");
			resp.getWriter().write(jsonResponse.toString());
			return ;
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}
}
