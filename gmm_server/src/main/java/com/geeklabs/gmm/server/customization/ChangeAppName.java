package com.geeklabs.gmm.server.customization;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.Log;

public final class ChangeAppName {
	private ChangeAppName() {}
	
	public static void changeAppName(String appName, String userAccount) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			String userAPKResStringPath = GMMServer.getUserAPKResStringPath(userAccount);
			Document doc = docBuilder.parse(new File(userAPKResStringPath));

			// Get the root element
			Node resources = doc.getFirstChild();

			// Get the string element by tag name directly
			Node string = resources.getFirstChild().getNextSibling();

			// update staff attribute
			string.setTextContent(appName);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(userAPKResStringPath));
			transformer.transform(source, result);

			Log.getLogger().info("Successfully updated app name for user: " + userAccount);

		} catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
			Log.getLogger().severe("Problem occurred while updating App name for user: " + userAccount + " problem: " + e.getMessage());
			throw new IllegalStateException(e.fillInStackTrace());
		}
	}

}
