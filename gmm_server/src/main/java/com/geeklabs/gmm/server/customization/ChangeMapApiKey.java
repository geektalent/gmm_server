package com.geeklabs.gmm.server.customization;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.Log;

public class ChangeMapApiKey {
	
	public static void changeMapApiKey(String key, String userAccount) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			String userAPKAndroidManifestPath = GMMServer.getUserAPKAndroidManifestPath(userAccount);
			Document doc = docBuilder.parse(userAPKAndroidManifestPath);
		
			// Get the staff element by tag name directly
			Node metadata = doc.getElementsByTagName("meta-data").item(0);

			// update staff attribute
			NamedNodeMap attr = metadata.getAttributes();
			Node nodeAttr = attr.getNamedItem("android:value");
			nodeAttr.setTextContent(key);

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(userAPKAndroidManifestPath));
			transformer.transform(source, result);
			
			Log.getLogger().info("Successfully updated api key for user: " + userAccount);
		} catch (ParserConfigurationException | TransformerException | IOException | SAXException e) {
			Log.getLogger().severe("Problem occurred while updating API key for user: " + userAccount + " problem: " + e.getMessage());
			throw new IllegalStateException(e.fillInStackTrace());
		}
	}
}
