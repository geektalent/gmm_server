package com.geeklabs.gmm.server.customization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.IOUtils;
import com.geeklabs.gmm.server.Log;
import com.geeklabs.gmm.server.StringUtils;
import com.google.common.io.Files;

public class ChangePackage {

	private ChangePackage() { }
	
	public static void changePackage(String userAccount, String packageName) throws FileNotFoundException, IOException {
		// Change AndroidManifest.xml
		replaceManifestPlaceHolders(userAccount, packageName);
		Log.getLogger().info("Package name has been updated in AndroidManifest.xml for user: " + userAccount);
		
		// Change Java sources
		replaceAllJavaSources(GMMServer.getJavaSourcePath(userAccount), packageName);
		
		Log.getLogger().info("Package name has been updated in 'src' folder for user: " + userAccount);
	}
	
	private static void replaceAllJavaSources(String javaSourcePath, String packageName) throws FileNotFoundException, IOException {
		 String[] extensions = {"java"};
		Collection<File> javaFiles = FileUtils.listFiles(new File(javaSourcePath), extensions, true);
		for (File javaFile : javaFiles) {
			replaceJavaPlaceHolder(javaFile.getPath(), packageName);
		}
	}

	private static void replaceManifestPlaceHolders(String userAccount, String packageName) throws FileNotFoundException, IOException {
		String userAPKAndroidManifestPath = GMMServer.getUserAPKAndroidManifestPath(userAccount);
		//Get complete content
		String androidManifestContent = IOUtils.toString(new FileInputStream(userAPKAndroidManifestPath)); 
		
		// Replace package="com.geeklabs.mapmaker"
		androidManifestContent = StringUtils.replaceAll(androidManifestContent, GMMServer.MANIFEST_PACKAGE, "package=\""+packageName+"\"");
		
		// Replace package="com.geeklabs.mapmaker.activity.permission.MAPS_RECEIVE"
		androidManifestContent = StringUtils.replaceAll(androidManifestContent, GMMServer.MANIFEST_PACKAGE_MAPS_RECEIVE, packageName + ".activity.permission.MAPS_RECEIVE");
		
		// Write back to file
		Files.write(androidManifestContent.getBytes(), new File(userAPKAndroidManifestPath));
		
	}
	
	private static void replaceJavaPlaceHolder(String filePath, String replaceTo) throws FileNotFoundException, IOException {
		String androidManifestContent = IOUtils.toString(new FileInputStream(filePath)); 
		androidManifestContent = StringUtils.replaceAll(androidManifestContent, GMMServer.JAVA_PACKAGE, replaceTo + ".R");
		Files.write(androidManifestContent.getBytes(), new File(filePath));
	}
}
