package com.geeklabs.gmm.server.customization;

import java.io.FileOutputStream;
import java.io.IOException;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.Log;

public class CreatingDrwableFolder {

	public static void createDrawableFolder(String userAccount, byte[] iconBytes) throws IOException {
		String userAPKResDrawablePath = GMMServer.getUserAPKResDrawablePath(userAccount);
		
		FileOutputStream outputStream = new FileOutputStream(userAPKResDrawablePath, false);
		outputStream.write(iconBytes);
		outputStream.flush();
		outputStream.close();	
		
		Log.getLogger().info("Successfully uploaded app icon for user: " + userAccount);
	}
}
