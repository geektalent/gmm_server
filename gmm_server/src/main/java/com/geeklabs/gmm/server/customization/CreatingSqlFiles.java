package com.geeklabs.gmm.server.customization;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.Log;

public class CreatingSqlFiles {

	public static void generateMarkingInfo(String userAccount,
			List<String> listOfMarkers) throws IOException {

		String apkMarkingInfoFilePath = GMMServer.getUserApkMarkingInfoFilePath(userAccount);
		BufferedWriter bW = new BufferedWriter(new FileWriter(apkMarkingInfoFilePath, false));

		for (String markingInfoQuery : listOfMarkers) {
			bW.write(markingInfoQuery);
			bW.newLine();
		}
		bW.flush();
		bW.close();
		Log.getLogger().info("Successfully created user specific markings sql data for user: " + userAccount);
	}

	public static void generateAppInfo(String userAccount, String appInfoQuery)
			throws IOException {
		String userApkAppInfoFilePath = GMMServer.getUserApkAppInfoFilePath(userAccount);
		BufferedWriter bW = new BufferedWriter(new FileWriter(userApkAppInfoFilePath, false));
		
		bW.write(appInfoQuery);
		
		bW.flush();
		bW.close();
		
		Log.getLogger().info("Successfully created user specific app sql '" +appInfoQuery+ "' data for user: " + userAccount);
	}

	public static void generateDrawBoundary(String userAccount, List<String> drawBoundaries) throws IOException {
		String apkDrawBoundaryFilePath = GMMServer.getUserApkDrawBoundaryInfoFilePath(userAccount);
		BufferedWriter bW = new BufferedWriter(new FileWriter(apkDrawBoundaryFilePath, false));

		for (String drawBoundary : drawBoundaries) {
			bW.write(drawBoundary);
			bW.newLine();
		}
		bW.flush();
		bW.close();
		Log.getLogger().info("Successfully created user specific draw area data for user: " + userAccount);
	}

}