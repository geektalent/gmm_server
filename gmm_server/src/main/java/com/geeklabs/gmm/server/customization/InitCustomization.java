package com.geeklabs.gmm.server.customization;

import java.io.File;
import java.io.IOException;

import net.lingala.zip4j.exception.ZipException;

import org.apache.commons.io.FileUtils;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.Log;

public class InitCustomization {

	public static void init(String userAccount) throws IOException, ZipException {
		String userApkDirPath = GMMServer.getUserApkRepoDirPath(userAccount);
		File userApkDir = new File(userApkDirPath);
		if (!userApkDir.exists()) {
			userApkDir.mkdir();
		}
		// copy GMMMapv2 to user apk repo
		File destApkPath = new File(GMMServer.getUserApkProjectDirPath(userAccount));
		FileUtils.copyDirectory(new File(GMMServer.GMM_MAPV2_SOURCE_PATH), destApkPath);
		
		// copy google-play-services_lib to user apk repo
		File googlePlayServiceLlibProjectDirPath = new File(GMMServer.getGooglePlayServiceLlibProjectDirPath(userAccount));
		FileUtils.copyDirectory(new File(GMMServer.GMM_GOOGLE_SER_LIB_SOURCE_PATH), googlePlayServiceLlibProjectDirPath);
				
		Log.getLogger().info("Successfully copied for user: " + userAccount);
	}
	
	public static void clean(String userAccount) {
		String userApkDirPath = GMMServer.getUserApkRepoDirPath(userAccount);
		File userApkDir = new File(userApkDirPath);
		if (userApkDir.exists()) {
			// Remove GMMMapv2 project folder
			String userApkProjectDirPath = GMMServer.getUserApkProjectDirPath(userAccount);
			File userApkProjectDir = new File(userApkProjectDirPath);
			if (userApkProjectDir.exists()) {
				FileUtils.deleteQuietly(userApkProjectDir);
				Log.getLogger().info("Successfully deleted user apk project folder for user: " + userAccount);
			}		
			
			// Remove GooglePlayServiceLlib project folder
			String googlePlayServiceLlibProjectDirPath = GMMServer.getGooglePlayServiceLlibProjectDirPath(userAccount);
			File googlePlayServiceLlibProjectDir = new File(googlePlayServiceLlibProjectDirPath);
			if (googlePlayServiceLlibProjectDir.exists()) {
				FileUtils.deleteQuietly(googlePlayServiceLlibProjectDir);
				Log.getLogger().info("Successfully deleted googlePlayServiceLlibProjectDir folder for user: " + userAccount);
			}	
		}
	}
}