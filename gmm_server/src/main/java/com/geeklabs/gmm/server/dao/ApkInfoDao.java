package com.geeklabs.gmm.server.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.geeklabs.gmm.server.domain.ApkInfo;

public interface ApkInfoDao {
	void save(ApkInfo apkInfoPojo, EntityManager entityManager);

	byte[] getApkFile(long apkId);

	List<ApkInfo> getApkList(long userId);
	
	ApkInfo getApk(long userID, long keyId);

}
