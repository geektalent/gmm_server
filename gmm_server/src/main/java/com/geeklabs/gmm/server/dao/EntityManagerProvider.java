package com.geeklabs.gmm.server.dao;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.hibernate.ejb.Ejb3Configuration;

public final class EntityManagerProvider {

	public static final EntityManagerProvider INSTANCE =  new EntityManagerProvider();
	
	private static EntityManagerFactory entityManagerFactory;

	private EntityManagerProvider() {
		if (entityManagerFactory == null) {
			Ejb3Configuration ejb3Configuration = new Ejb3Configuration();
			Ejb3Configuration configure = ejb3Configuration.configure("geeklabs", getDbConfig());
			entityManagerFactory = configure.buildEntityManagerFactory();
		}
	}

	public EntityManager getEntityManager() {
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		return entityManager;

	}

	private Map<String, Object> getDbConfig() {

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("javax.persistence.jdbc.driver", "com.mysql.jdbc.Driver");
		map.put("javax.persistence.jdbc.user", "root");
		map.put("javax.persistence.jdbc.password", "gmm");
		map.put("javax.persistence.jdbc.url", "jdbc:mysql://localhost:3306/serverdb");
		
		map.put("hibernate.c3p0.idleConnectionTestPeriod", "60");	
		// Defines how many times c3p0 will try to acquire a new Connection from the database before giving up
		map.put("hibernate.c3p0.acquireRetryAttempts", "1");
		map.put("hibernate.c3p0.preferredTestQuery", "select 1;");
		return map;
	}

}
