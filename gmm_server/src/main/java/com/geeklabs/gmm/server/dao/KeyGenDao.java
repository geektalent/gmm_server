package com.geeklabs.gmm.server.dao;

import java.util.List;

import com.geeklabs.gmm.server.domain.KeyStore;

public interface KeyGenDao {

	public abstract void save(KeyStore keyStore);

	List<KeyStore> retrieve(int id);

	KeyStore getKeyStoreByName(int i, String keyStoreName);
	
	KeyStore getDefaultKeyStore();

}
