package com.geeklabs.gmm.server.dao;

import javax.persistence.EntityManager;

import com.geeklabs.gmm.server.domain.User;

public interface UserDao {	
	void save(User user, EntityManager entityManager);
	User getUser(String email, EntityManager entityManager);
}
