package com.geeklabs.gmm.server.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.geeklabs.gmm.server.dao.ApkInfoDao;
import com.geeklabs.gmm.server.dao.EntityManagerProvider;
import com.geeklabs.gmm.server.domain.ApkInfo;

public class ApkInfoDaoImpl implements ApkInfoDao {

	public static final ApkInfoDao INSTANCE = new ApkInfoDaoImpl();

	private ApkInfoDaoImpl() {
	}

	@Override
	public void save(ApkInfo apkInfoPojo, EntityManager entityManager) {
		entityManager.getTransaction().begin();
		entityManager.persist(apkInfoPojo);
		entityManager.getTransaction().commit();
	}

	@Override
	public byte[] getApkFile(long apkId) {
		EntityManager entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
		try {
			ApkInfo apkInfo = entityManager.find(ApkInfo.class, apkId);
			return apkInfo.getApkFileInfo();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<ApkInfo> getApkList(long userID) {
		EntityManager entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<ApkInfo> query = criteriaBuilder.createQuery(ApkInfo.class);

			Root<ApkInfo> from = query.from(ApkInfo.class);
			query.select(from);

			Predicate userequal = criteriaBuilder.equal(from.get("userID"), userID);
			
			query.where(userequal);

			List<ApkInfo> apkList = entityManager.createQuery(query).getResultList();

			return apkList;
		} finally {
			entityManager.close();
		}
	}
	
	@Override
	public ApkInfo getApk(long userID, long keyId) {
		EntityManager entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<ApkInfo> query = criteriaBuilder.createQuery(ApkInfo.class);

			Root<ApkInfo> from = query.from(ApkInfo.class);
			query.select(from);

			Predicate userequal = criteriaBuilder.equal(from.get("userID"), userID);
			Predicate keyEqual = criteriaBuilder.equal(from.get("keyId"), keyId);
			Predicate andPrerdicate = criteriaBuilder.and(userequal, keyEqual);
			query.where(andPrerdicate);

			List<ApkInfo> apkList = entityManager.createQuery(query).getResultList();
			if (apkList != null && !apkList.isEmpty()) {
				return apkList.get(0); // return first , to read package name and apikey
			}
			return null;
		} finally {
			entityManager.close();
		}
	}
}
