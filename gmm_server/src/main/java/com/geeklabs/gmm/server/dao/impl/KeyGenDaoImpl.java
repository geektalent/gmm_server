package com.geeklabs.gmm.server.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.geeklabs.gmm.server.GMMServer;
import com.geeklabs.gmm.server.dao.EntityManagerProvider;
import com.geeklabs.gmm.server.dao.KeyGenDao;
import com.geeklabs.gmm.server.domain.KeyStore;

public class KeyGenDaoImpl implements KeyGenDao {

	public static final KeyGenDao INSTANCE = new KeyGenDaoImpl();

	private KeyGenDaoImpl() {
	}

	@Override
	public void save(KeyStore keyStore) {
		// call EntityManager
		EntityManager entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(keyStore);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<KeyStore> retrieve(int userID) {
		EntityManager entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<KeyStore> query = criteriaBuilder.createQuery(KeyStore.class);

			Root<KeyStore> from = query.from(KeyStore.class);
			query.select(from);

			Predicate userequal = criteriaBuilder.equal(from.get("userId"), userID);
			query.where(userequal);

			List<KeyStore> keystoreList = entityManager.createQuery(query).getResultList();

			return keystoreList;
		} finally {
			entityManager.close();
		}
	}

	@Override
	/**
	 * Gets the Keystore blob from Keystore table based on user id and keystore name.
	 */
	public KeyStore getKeyStoreByName(int userID, String keyStoreName) {

		EntityManager entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<KeyStore> query = criteriaBuilder.createQuery(KeyStore.class);

			Root<KeyStore> from = query.from(KeyStore.class);
			query.select(from);

			Predicate userequal = criteriaBuilder.equal(from.get("userId"), userID);
			Predicate keyEqual = criteriaBuilder.equal(from.get("keyStoreName"), keyStoreName);
			Predicate andPrerdicate = criteriaBuilder.and(userequal, keyEqual);
			query.where(andPrerdicate);

			KeyStore singleResult = entityManager.createQuery(query).getSingleResult();

			return singleResult;
		} finally {
			entityManager.close();
		}
	}

	@Override
	/**
	 * Gets the Keystore blob from Keystore table based on user id and keystore name.
	 */
	public KeyStore getDefaultKeyStore() {

		EntityManager entityManager = EntityManagerProvider.INSTANCE.getEntityManager();
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<KeyStore> query = criteriaBuilder.createQuery(KeyStore.class);

			Root<KeyStore> from = query.from(KeyStore.class);
			query.select(from);

			Predicate userequal = criteriaBuilder.equal(from.get("keyStoreName"), GMMServer.TEST_KEY);
			query.where(userequal);

			KeyStore singleResult = entityManager.createQuery(query).getSingleResult();

			return singleResult;
		} finally {
			entityManager.close();
		}
	}
}
