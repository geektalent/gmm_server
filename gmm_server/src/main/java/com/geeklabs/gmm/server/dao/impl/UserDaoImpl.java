package com.geeklabs.gmm.server.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.geeklabs.gmm.server.dao.UserDao;
import com.geeklabs.gmm.server.domain.User;

public final class UserDaoImpl implements UserDao {

	public static final UserDao INSTANCE = new UserDaoImpl();

	private UserDaoImpl() {
	}

	@Override
	public User getUser(String email, EntityManager entityManager) {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);

			Root<User> from = query.from(User.class);
			query.select(from);

			Predicate userequal = criteriaBuilder.equal(from.get("email"), email);
			query.where(userequal);

			List<User> resultList = entityManager.createQuery(query).getResultList();

			if (resultList != null && !resultList.isEmpty()) {
				return resultList.get(0);
			}
			return null;
	}

	@Override
	public void save(User user, EntityManager entityManager) {
		entityManager.getTransaction().begin();
		entityManager.merge(user);
		entityManager.getTransaction().commit();
	}
}
