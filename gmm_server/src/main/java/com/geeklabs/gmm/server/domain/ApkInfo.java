package com.geeklabs.gmm.server.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "apkinfo")
public class ApkInfo {

	@Id
	@GeneratedValue
	private long apkID;

	@ForeignKey(name = "userid")
	private long userID;
	
	@ForeignKey(name = "keyId")
	private long keyId;
	
	private String apkName;
	private String packageName;
	private String apiKey;
	
	@Lob
	private byte[] apkFileInfo;

	public long getApkID() {
		return apkID;
	}

	public void setApkID(long apkID) {
		this.apkID = apkID;
	}

	public long getUserID() {
		return userID;
	}

	public void setUserID(long userID) {
		this.userID = userID;
	}

	public String getApkName() {
		return apkName;
	}

	public void setApkName(String apkName) {
		this.apkName = apkName;
	}
	public byte[] getApkFileInfo() {
		return apkFileInfo;
	}

	public void setApkFileInfo(byte[] apkFileInfo) {
		this.apkFileInfo = apkFileInfo;
	}

	public long getKeyId() {
		return keyId;
	}

	public void setKeyId(long keyId) {
		this.keyId = keyId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	public String getApiKey() {
		return apiKey;
	}
	
}
