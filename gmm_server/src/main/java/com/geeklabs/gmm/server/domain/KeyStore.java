package com.geeklabs.gmm.server.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "KeyStore")
public class KeyStore {

	@Id
	@GeneratedValue
	private long keyId;

	@ForeignKey(name = "userid")
	private long userId;

	@Lob
	private byte[] keyStoreFile;

	private String keyStoreName;

	private String sha1key;
	
	public String getSha1key() {
		return sha1key;
	}

	public void setSha1key(String sha1key) {
		this.sha1key = sha1key;
	}

	public String getkeyStoreName() {
		return keyStoreName;
	}

	public void setkeyStoreName(String keyStoreName) {
		this.keyStoreName = keyStoreName;
	}

	public long getKeyId() {
		return keyId;
	}

	public void setKeyId(long keyId) {
		this.keyId = keyId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public byte[] getKeyStoreFile() {
		return keyStoreFile;
	}

	public void setKeyStoreFile(byte[] keyStoreFile) {
		this.keyStoreFile = keyStoreFile;
	}

}
