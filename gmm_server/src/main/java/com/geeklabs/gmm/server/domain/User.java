package com.geeklabs.gmm.server.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userinfo")
public class User {
	@Id
	@GeneratedValue
	private int id;
	
	private String email;
	private String password;
	private boolean isTestKeyUsed;
	
	public void setTestKeyUsed(boolean isTestKeyUsed) {
		this.isTestKeyUsed = isTestKeyUsed;
	}
	
	public boolean isTestKeyUsed() {
		return isTestKeyUsed;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
