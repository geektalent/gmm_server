package com.geeklabs.gmm.server.jarsigner;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.Signature;
import java.security.SignatureException;

/** Write to another stream and also feed it to the Signature object. */

public class SignatureOutputStream extends FilterOutputStream {
	private Signature mSignature;
	private int mCount;

	public SignatureOutputStream(OutputStream out, Signature sig) {
		super(out);
		mSignature = sig;
		mCount = 0;
	}

	@Override
	public void write(int b) throws IOException {
		try {
			mSignature.update((byte) b);
		} catch (SignatureException e) {
			throw new IOException("SignatureException: " + e);
		}
		super.write(b);
		mCount++;
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		try {
			mSignature.update(b, off, len);
		} catch (SignatureException e) {
			throw new IOException("SignatureException: " + e);
		}
		super.write(b, off, len);
		mCount += len;
	}

	public int size() {
		return mCount;
	}
}