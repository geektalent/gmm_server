package com.geeklabs.gmm.server.jarsigner.keys;

import java.security.MessageDigest;

import org.bouncycastle.util.encoders.HexTranslator;

import com.geeklabs.gmm.server.Log;

public class FingerPrint {

	static byte[] calcDigest(String algorithm, byte[] encodedCert) {
		byte[] result = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
			messageDigest.update(encodedCert);
			result = messageDigest.digest();
		} catch (Exception x) {
			x.printStackTrace();
			Log.getLogger().severe("Problem occurred while calculating digest" + x.getMessage());
		}
		return result;
	}

	public static String hexFingerprint(String algorithm, byte[] encodedCert) {
		try {
			byte[] digest = calcDigest(algorithm, encodedCert);
			if (digest == null)
				return null;
			HexTranslator hexTranslator = new HexTranslator();
			byte[] hex = new byte[digest.length * 2];
			hexTranslator.encode(digest, 0, digest.length, hex, 0);
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < hex.length; i += 2) {
				builder.append((char) hex[i]);
				builder.append((char) hex[(i + 1)]);
				if (i == hex.length - 2)
					continue;
				builder.append(':');
			}
			return builder.toString().toUpperCase();
		} catch (Exception x) {
			x.printStackTrace();
			Log.getLogger().severe("Problem occurred while converting to sha1 digest" + x.getMessage());
		}
		return null;
	}
}
