package com.geeklabs.gmm.server.jarsigner.keys;

import static com.geeklabs.gmm.server.GMMServer.KEY_PWD;
import static com.geeklabs.gmm.server.GMMServer.KEY_STORE_PWD;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import com.geeklabs.gmm.server.Log;
import com.geeklabs.gmm.server.pojo.KeyInfo;

public class KeyToolManager {

	private static final String CERT_SIGN_ALG = "MD5WithRSAEncryption";

	public com.geeklabs.gmm.server.domain.KeyStore keyGenerator(final KeyInfo keyInfo) throws Exception {
		try {
			//
			Security.addProvider(new BouncyCastleProvider());

			// Generate a key pair using "RSA" public-key cryptography algorithm
			// and a key size of 1024.
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA", "BC");
			keyPairGenerator.initialize(1024, new SecureRandom());

			// Get key pair from KeyPairGenerator
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			PublicKey pubKey = keyPair.getPublic();

			// Get distinguished name values for certificate
			Hashtable<ASN1ObjectIdentifier, String> dnValues = getDNValues(keyInfo);
			// Get distinguished name values for certificate
			Vector<ASN1ObjectIdentifier> dnList = getDNAsListValues();

			// Instantiate an X.509 cert. generator.
			
			X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
			// Start creating the certificate
			certGen.setSerialNumber(BigInteger.valueOf(1));
			certGen.setIssuerDN(new X509Principal(dnList, dnValues));
			certGen.setNotBefore(new Date(System.currentTimeMillis()));
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.YEAR, keyInfo.getCertValidityYears());
			certGen.setNotAfter(calendar.getTime());
			certGen.setSubjectDN(new X509Principal(dnList, dnValues));

			// Set the public key of the key pair and the signing algorithm to
			// the cert generator.
			certGen.setPublicKey(pubKey);
			certGen.setSignatureAlgorithm(CERT_SIGN_ALG);

			X509Certificate pkCertificate = certGen.generateX509Certificate(keyPair.getPrivate());

			// extract sha1 from certificate
			byte[] encodedCert = pkCertificate.getEncoded();
			String keySha1 = FingerPrint.hexFingerprint("SHA1", encodedCert);

			//KeyStore privateKS = KeyStore.getInstance("bks", new BouncyCastleProvider());
			KeyStore privateKS = KeyStore.getInstance("JKS");// JKS
			privateKS.load(null, null);

			// Import the private key to the key store.
			privateKS.setKeyEntry(keyInfo.getKeystoreName(), keyPair.getPrivate(), KEY_PWD.toCharArray(),
					new java.security.cert.Certificate[] { pkCertificate });

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			privateKS.store(outputStream, KEY_STORE_PWD.toCharArray());
			
			// Create a Key store DAO object 
			com.geeklabs.gmm.server.domain.KeyStore store = new com.geeklabs.gmm.server.domain.KeyStore();
			store.setkeyStoreName(keyInfo.getKeystoreName());
			store.setKeyStoreFile(outputStream.toByteArray());
			store.setSha1key(keySha1);
			
			outputStream.flush();
			outputStream.close();
			
			return store;
		} catch (RuntimeException | NoSuchAlgorithmException | NoSuchProviderException | InvalidKeyException | SignatureException
				| IOException | KeyStoreException | CertificateException e) {
			Log.getLogger().severe("Problem occurred while creating key store: with name '" +keyInfo.getKeystoreName() + "'");
			throw e;
		}
	}

	@SuppressWarnings("deprecation")
	private static Vector<ASN1ObjectIdentifier> getDNAsListValues() {
		Vector<ASN1ObjectIdentifier> dnList = new Vector<>();
		dnList.add(X509Principal.COUNTRY_OF_RESIDENCE);
		dnList.add(X509Principal.O);
		dnList.add(X509Principal.L);
		dnList.add(X509Principal.ST);
		dnList.add(X509Principal.E);
		dnList.add(X509Principal.CN);
		dnList.add(X509Principal.OU);
		dnList.add(X509Principal.STREET);
		return dnList;
	}

	@SuppressWarnings("deprecation")
	private static Hashtable<ASN1ObjectIdentifier, String> getDNValues(KeyInfo keyInfo) {
		Hashtable<ASN1ObjectIdentifier, String> dnValues = new Hashtable<>();
		dnValues.put(X509Principal.COUNTRY_OF_RESIDENCE, keyInfo.getCountryCode());
		dnValues.put(X509Principal.CN, keyInfo.getCommonName());
		dnValues.put(X509Principal.O, keyInfo.getOrganization());
		dnValues.put(X509Principal.OU, keyInfo.getOrganizationalUnit());
		dnValues.put(X509Principal.STREET, keyInfo.getStreet());
		dnValues.put(X509Principal.L, keyInfo.getLocality());
		dnValues.put(X509Principal.ST, keyInfo.getState());
		dnValues.put(X509Principal.E, keyInfo.getEmail());

		return dnValues;
	}
}
