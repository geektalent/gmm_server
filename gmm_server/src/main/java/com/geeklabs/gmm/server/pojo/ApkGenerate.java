package com.geeklabs.gmm.server.pojo;

import java.util.List;

import com.google.common.collect.Lists;

public class ApkGenerate {
	private String keyStoreName;
	private String appName;
	private String apiKey;
	private byte[] apkFile;
	private List<String> markingInfoQueries = Lists.newArrayList();
	private String appInfoQuery;
	private String packageName;
	private List<String> drawBoundaries = Lists.newArrayList();
	
	public String getPackageName() {
		return packageName;
	}
	
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	public List<String> getMarkingInfoQueries() {
		return markingInfoQueries;
	}

	public void setMarkingInfoQueries(List<String> markingInfoQueries) {
		this.markingInfoQueries = markingInfoQueries;
	}

	public String getAppInfoQuery() {
		return appInfoQuery;
	}

	public void setAppInfoQuery(String appInfoQuery) {
		this.appInfoQuery = appInfoQuery;
	}

	public byte[] getApkFile() {
		return apkFile;
	}

	public void setApkFile(byte[] apkFile) {
		this.apkFile = apkFile;
	}

	public String getKeyStoreName() {
		return keyStoreName;
	}

	public void setKeyStoreName(String keyStoreName) {
		this.keyStoreName = keyStoreName;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public List<String> getDrawBoundaries() {
		return drawBoundaries;
	}
	
	public void setDrawBoundaries(List<String> drawBoundaries) {
		this.drawBoundaries = drawBoundaries;
	}
	
	@Override
	public String toString() {
		return "API key:" + apiKey + "App Name: " + appName + 
				"Key Store Name: " + keyStoreName + 
				"Package Name:" + packageName;
	}
}
