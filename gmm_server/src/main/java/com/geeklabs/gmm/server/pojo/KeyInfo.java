package com.geeklabs.gmm.server.pojo;

public class KeyInfo {
	private String keystoreName = null;
	private int certValidityYears = 30;
	private String commonName = "";
	private String organizationalUnit = "";
	private String organization = "";
	private String street = "";
	private String locality = "";
	private String state= "";
	private String countryCode = "";
	private String email = "";
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getKeystoreName() {
		return keystoreName;
	}
	public void setKeystoreName(String keystoreName) {
		this.keystoreName = keystoreName;
	}
	public int getCertValidityYears() {
		return certValidityYears;
	}
	public void setCertValidityYears(int certValidityYears) {
		this.certValidityYears = certValidityYears;
	}
	public String getCommonName() {
		return commonName;
	}
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}
	public String getOrganizationalUnit() {
		return organizationalUnit;
	}
	public void setOrganizationalUnit(String organizationalUnit) {
		this.organizationalUnit = organizationalUnit;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
